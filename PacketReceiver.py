import ast
import asyncio
import configparser
import json
import random
import time

from kafka import KafkaProducer

class PacketReceiver:
    def __init__(self, bootstrap_servers, received_packets_topic, sasl_mechanism, security_protocol,
                 sasl_plain_username, sasl_plain_password, acks):
        self.bootstrap_servers = bootstrap_servers
        self.received_packets_topic = received_packets_topic
        self.sasl_mechanism = sasl_mechanism
        self.security_protocol = security_protocol
        self.sasl_plain_username = sasl_plain_username
        self.sasl_plain_password = sasl_plain_password
        self.acks = acks

        # Create a Kafka producer with acks = all so that we are sure the data packet has been pushed to leader 
        # and in sync followers so that we are sure packet reached the broker
        self.producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers,
                                      sasl_plain_username = self.sasl_plain_username,
                                      sasl_plain_password = self.sasl_plain_password,
                                      security_protocol = self.security_protocol,
                                      sasl_mechanism = self.sasl_mechanism,
                                      acks=self.acks
                                      )
        
        # Create dictionary to track the last chunk arrival for each Primary Resource ID
        self.last_chunk_tracker = {}

        # Max out of order time for a packet
        self.last_chunk_timeout = 60
    
    def receive_data_packet(self, packet):
        # Send the data packet to the Kafka topic
        self.producer.send(self.received_packets_topic, json.dumps(packet).encode('utf-8'))
        self.producer.flush()

        print (f"Pushed data packet: {packet} to Kafka topic {self.received_packets_topic}")

        primary_resource_id = packet["primary_resource_id"]
        last_chunk_flag = packet["last_chunk_flag"]

        if last_chunk_flag:
            # Set a timer for the max out of order time for a packet. 
            # Once we receive the last packet, we can be sure that all the mising packets will received within the max out of order time.
            if primary_resource_id not in self.last_chunk_tracker:
                self.last_chunk_tracker[primary_resource_id] = asyncio.ensure_future(self.wait_for_last_chunk(primary_resource_id))

    async def wait_for_last_chunk(self, primary_resource_id):
        # Wait for the specified duration
        await asyncio.sleep(self.last_chunk_timeout)

        if primary_resource_id in self.last_chunk_tracker:
            # Remove the Primary Resource ID from the tracker
            del self.last_chunk_tracker[primary_resource_id]
    
    def run(self):
        # Read the incoming stream of packets from the input file
        packets_file = open('input_packets\input_packets2.txt', 'r')
        lines = packets_file.read().split('\n')
        for line in lines:
            if line != '':
                packet = ast.literal_eval(line)

                # simulating random time between packets
                time.sleep(random.random())
                self.receive_data_packet(packet)

# Read configuration from config.ini file
config = configparser.ConfigParser()
config.read('config.ini')

# Get values from the configuration
bootstrap_servers = config.get('General', 'bootstrap_servers')
received_packets_topic = config.get('General', 'received_packets_topic')
sasl_mechanism = config.get('General', 'sasl_mechanism')
security_protocol = config.get('General', 'security_protocol')
sasl_plain_username = config.get('General', 'sasl_plain_username')
sasl_plain_password = config.get('General', 'sasl_plain_password')
acks = config.get('General', 'acks')

# Create an instance of the PacketReceiver class
packet_receiver = PacketReceiver(bootstrap_servers, received_packets_topic, sasl_mechanism,
                                 security_protocol, sasl_plain_username, sasl_plain_password, acks)

# Run the packet receiver
packet_receiver.run()
