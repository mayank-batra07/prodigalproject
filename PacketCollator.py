import ast
import asyncio
import configparser
import json
import threading

from kafka import KafkaConsumer

class PacketCollator:
    def __init__(self, bootstrap_servers, processed_packets_topic, sasl_mechanism, security_protocol,
                 sasl_plain_username, sasl_plain_password, auto_offset_reset):
        self.bootstrap_servers = bootstrap_servers
        self.processed_packets_topic = processed_packets_topic
        self.sasl_mechanism = sasl_mechanism
        self.security_protocol = security_protocol
        self.sasl_plain_username = sasl_plain_username
        self.sasl_plain_password = sasl_plain_password
        self.auto_offset_reset = auto_offset_reset

        # Create a Kafka consumer
        self.consumer = KafkaConsumer(self.processed_packets_topic,
                                      bootstrap_servers=bootstrap_servers,
                                      sasl_plain_username = self.sasl_plain_username,
                                      sasl_plain_password = self.sasl_plain_password,
                                      security_protocol = self.security_protocol,
                                      sasl_mechanism = self.sasl_mechanism,
                                      auto_offset_reset=self.auto_offset_reset
                                      )
        
        # Create dictionary to store processed packets for each Primary Resource ID
        self.processed_packets = {}

        # Create dictionary to store number of expected packets for each Primary Resource ID
        self.number_of_packets_expected = {}

        # Create a lock
        self.lock = threading.Lock()

    async def collate_packets(self):
        for message in self.consumer:
            with self.lock:
                # Extract data from the received message
                processed_packet = dict(json.loads(message.value.decode('utf-8')))
                primary_resource_id = processed_packet["primary_resource_id"]
                processed_data = processed_packet["processed_data"]
                packet_index = processed_packet["packet_index"]
                last_chunk_flag = processed_packet["last_chunk_flag"]

                # Check if the Primary Resource ID exists in the dictionary
                if primary_resource_id not in self.processed_packets:
                    self.processed_packets[primary_resource_id] = []    

                # Append the processed data packet to the list for the Primary Resource ID
                self.processed_packets[primary_resource_id].append((processed_data, int(packet_index)))

                # Check if it's the last chunk for the Primary Resource ID
                # Index of the last chunk is the number of packets for the Primary Resource ID 
                if last_chunk_flag:
                    self.number_of_packets_expected[primary_resource_id] = packet_index

                # When the number of packets received for the Primary Resource ID is equal to the packet index of the last chunk, we can send it to the downstream service
                if self.number_of_packets_expected and \
                    primary_resource_id in self.number_of_packets_expected.keys() and \
                    len(self.processed_packets[primary_resource_id]) == self.number_of_packets_expected[primary_resource_id]:  
                    # Sort the packets based on the packet index
                    self.processed_packets[primary_resource_id].sort(key=lambda x: x[1])

                    # Collate the packets into a single data packet
                    collated_data_packet = []

                    for packet in self.processed_packets[primary_resource_id]:
                        collated_data_packet = collated_data_packet + ast.literal_eval(str(packet[0]))

                    # Trigger downstream processing (e.g., by calling a webhook)
                    self.trigger_downstream_processing(primary_resource_id, collated_data_packet)

                    # Remove the processed packets for the Primary Resource ID from the dictionary
                    del self.processed_packets[primary_resource_id]
                    del self.number_of_packets_expected[primary_resource_id]

    def trigger_downstream_processing(self, primary_resource_id, collated_data_packet):
        # Simulate downstream processing by writing the collated data to a file
        filename = f"output_{primary_resource_id}.txt"
        with open(filename, 'w') as file:
            for data in collated_data_packet:
                file.write(str(data) + '\n')
        print (f"Collated data packet for primary resource id {primary_resource_id} written to file {filename}")
    
    def run(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.collate_packets())


# Read configuration from config.ini file
config = configparser.ConfigParser()
config.read('config.ini')

# Get values from the configuration
bootstrap_servers = config.get('General', 'bootstrap_servers')
processed_packets_topic = config.get('General', 'processed_packets_topic')
sasl_mechanism = config.get('General', 'sasl_mechanism')
security_protocol = config.get('General', 'security_protocol')
sasl_plain_username = config.get('General', 'sasl_plain_username')
sasl_plain_password = config.get('General', 'sasl_plain_password')
auto_offset_reset = config.get('General', 'auto_offset_reset')

# Create an instance of the PacketCollator class
packet_collator = PacketCollator(bootstrap_servers, processed_packets_topic, sasl_mechanism,
                                 security_protocol, sasl_plain_username, sasl_plain_password, auto_offset_reset)

# Run the packet collation
packet_collator.run()
