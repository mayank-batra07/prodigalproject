import asyncio
import configparser
import json

from kafka import KafkaConsumer, KafkaProducer

class PacketConsumer:
    def __init__(self, bootstrap_servers, received_packets_topic, processed_packets_topic, sasl_mechanism, security_protocol,
                 sasl_plain_username, sasl_plain_password, auto_offset_reset, acks):
        self.bootstrap_servers = bootstrap_servers
        self.received_packets_topic = received_packets_topic
        self.processed_packets_topic = processed_packets_topic
        self.sasl_mechanism = sasl_mechanism
        self.security_protocol = security_protocol
        self.sasl_plain_username = sasl_plain_username
        self.sasl_plain_password = sasl_plain_password
        self.auto_offset_reset = auto_offset_reset
        self.acks = acks

        # Create a Kafka producer
        self.producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers,
                                      sasl_plain_username = self.sasl_plain_username,
                                      sasl_plain_password = self.sasl_plain_password,
                                      security_protocol = self.security_protocol,
                                      sasl_mechanism = self.sasl_mechanism,
                                      acks = acks
                                      )

        # Create a Kafka consumer
        self.consumer = KafkaConsumer(self.received_packets_topic, 
                                      bootstrap_servers=self.bootstrap_servers,
                                      sasl_plain_username = self.sasl_plain_username,
                                      sasl_plain_password = self.sasl_plain_password,
                                      security_protocol = self.security_protocol,
                                      sasl_mechanism = self.sasl_mechanism,
                                      auto_offset_reset=self.auto_offset_reset
                                      )
    
    async def process_data_packets(self):
        for message in self.consumer:
            # Extract data from the received message
            data_packet = dict(json.loads(message.value.decode('utf-8')))

            # Simulate the ancillary service by processing the payload
            processed_data = self.process_ancillary_service(data_packet["payload"])
            
            processed_packet = {
                "primary_resource_id": data_packet["primary_resource_id"],
                "processed_data": processed_data,
                "packet_index": data_packet["packet_index"],
                "last_chunk_flag": data_packet["last_chunk_flag"]
                }
            
            # Push the processed data back to the broker
            self.producer.send(self.processed_packets_topic, json.dumps(processed_packet).encode('utf-8'))
            self.producer.flush()

            print (f"Processed and pushed data packet: {processed_packet} to Kafka topic {self.processed_packets_topic}")

    def process_ancillary_service(self, payload):
        # Simulate the ancillary service by splitting the payload into words
        words = payload.split()
        processed_data = [len(word) for word in words]
        return processed_data
    
    def run(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.process_data_packets())


# Read configuration from config.ini file
config = configparser.ConfigParser()
config.read('config.ini')

# Get values from the configuration
bootstrap_servers = config.get('General', 'bootstrap_servers')
received_packets_topic = config.get('General', 'received_packets_topic')
processed_packets_topic = config.get('General', 'processed_packets_topic')
sasl_mechanism = config.get('General', 'sasl_mechanism')
security_protocol = config.get('General', 'security_protocol')
sasl_plain_username = config.get('General', 'sasl_plain_username')
sasl_plain_password = config.get('General', 'sasl_plain_password')
auto_offset_reset = config.get('General', 'auto_offset_reset')
acks = config.get('General', 'acks')

# Create an instance of the PacketConsumer class
packet_consumer = PacketConsumer(bootstrap_servers, received_packets_topic, processed_packets_topic, sasl_mechanism,
                                 security_protocol, sasl_plain_username, sasl_plain_password, auto_offset_reset, acks)

# Run the packet consumer
packet_consumer.run()
        
